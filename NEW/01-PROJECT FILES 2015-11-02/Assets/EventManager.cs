﻿using UnityEngine;
using System.Collections;

public class EventManager : MonoBehaviour {


	public delegate void ChangeGameState(GameController.GameStates state);
	public static  ChangeGameState onChangeGameStateEvent;
	
	
	public static void ChangeGameStateMethod(GameController.GameStates state){
		if (onChangeGameStateEvent != null) {
			onChangeGameStateEvent(state);
		}
	}

}
