﻿using UnityEngine;
using System.Collections;

public class BackGround1Class : PlatformClass {



	float _parallaxSpeed = 0.05f;

	// Use this for initialization
	void Start () {

	}
	
	protected override void Update () {
		switch(GameController._currentGameState){
		case GameController.GameStates.InGame:
			transform.position -= Vector3.right * GameController._gameXSpeed *_parallaxSpeed* Time.deltaTime;		
			if(transform.position.x < -15f)
				this.Recycle();
			break;
		}
	}
}
