﻿ using UnityEngine;
using System.Collections;

public class PlatformsManagers : MonoBehaviour {
	public EnemyClass _enemyPrefab;
	public EnemyClass _enemyBPrefab;

	public CoinClass _coinPrefab;
	public StarClass _starPrefab;

	public GameObject _platformPrefabs;


	public GameObject _lastPlatformPrefabs;
	public GameObject _curPlatformPrefabs;

	private float _CreatePlatformCounter ;
	private float _CreatePlatformCounterBase = 1f;
	private Vector3 _vtemp;
	private float[] _elevations = {
		-2.5f,
		-2f,
		-1.5f,
		-1f,
		0f,
		0.5f,
		1f
	};
	private float _spawnx = 8f;
	private	int _rnd = 0;
	private int _lstRnd= 0;

	private float _currElevation  ;
	public float _eCounter ;
	public float _cCounter ;
	private int _coinSpawnPackage;
	public int _coinCount;
	private  void Awake(){
		_CreatePlatformCounter = _CreatePlatformCounterBase;
		_platformPrefabs.CreatePool ();
		_enemyPrefab.CreatePool ();
		_enemyBPrefab.CreatePool ();
		_currElevation = _elevations [0];
		_eCounter = 1f;//Random.Range (5f, 20f);
		_cCounter = Random.Range (5f, 20f);

	}


	// Use this for initialization
	private  void Start () {
	
	}
	private float spawnPit(){
		float rnd = Random.Range (0f, 100f);
		switch (GameController._currGameSpeedMode) {
		case GameController.GameSpeedMode.Slow:
				if(rnd <80)
					return 1f;
				else 
					return 2f;
			break;
		case GameController.GameSpeedMode.Fast:
			if(rnd <60)
				return 1f;
			else if(rnd <80)
				return 2f;
			else 
				return 3f;

			break;
		case GameController.GameSpeedMode.Faster:
			if(rnd <40)
				return 1f;
			else if(rnd <60)
				return 2f;
			else if(rnd <80)
				return 3f;
			else 
				return 4f;

			break;
		case GameController.GameSpeedMode.Crazy:
			if(rnd <20)
				return 1f;
			else if(rnd <40)
				return 2f;
			else if(rnd <60)
				return 3f;
			else if(rnd <80)
				return 4f;
			else 
				return 5f;
			break;

		}
		return 1f;
	}
	bool _pit = false;
	// Update is called once per frame
	private void Update () {
		_eCounter-= 1f*Time.deltaTime;
		_cCounter-= 1f*Time.deltaTime;
		_CreatePlatformCounter -= GameController._gameXSpeed * Time.deltaTime;
		//Debug.Log ("_CreatePlatformCounter " + _CreatePlatformCounter + " _currElevation " +_currElevation);
		//ganti posisi y 
		if (_CreatePlatformCounter <= 0) {
			_CreatePlatformCounter = Random.Range(5f, _CreatePlatformCounterBase);
			_rnd = Random.Range(0,3);
			_currElevation = _elevations[_rnd];

			switch(_lstRnd){
				case 2:
					_rnd = Random.Range(0,4);
					_currElevation = _elevations[_rnd];
					//Debug.Log ("_currElevation " + _rnd + " ===========================");
					break;
				case 3:
					_rnd = Random.Range(0,5);
					_currElevation = _elevations[_rnd];
					//Debug.Log ("_currElevation " + _rnd + " ===========================");
					break;
				case 4:
					_rnd = Random.Range(0,6);
					_currElevation = _elevations[_rnd];
					//Debug.Log ("_currElevation " + _rnd + " ===========================");
					break;
				case 5:
					_rnd = Random.Range(0,7);
					_currElevation = _elevations[_rnd];
					//Debug.Log ("_currElevation " + _rnd + " ===========================");
					break;
				case 6:
					_rnd = Random.Range(0,3);
					_currElevation = _elevations[_rnd];
					//Debug.Log ("_currElevation " + _rnd + " ===========================");
					break;
			}
			
			if(_lstRnd > _currElevation){
				_cCounter = Random.Range (0.3f, 0.5f);
			}
			//Debug.Log ("_currElevation " + _currElevation + " ===========================");
			_lstRnd = _rnd;
		}


		if(_lastPlatformPrefabs==null){
			_curPlatformPrefabs = _platformPrefabs.Spawn(new Vector3(_spawnx,_currElevation,0f));
		}else{
			if(_lastPlatformPrefabs.transform.position.x < 10f-1.1f){
				_curPlatformPrefabs = _platformPrefabs.Spawn(new Vector3(_spawnx,_currElevation,0f));



				_vtemp = _lastPlatformPrefabs.transform.position;


				int pit = Random.Range(0,100);

				if(pit>90 && !_pit){
					//Lobang

					_vtemp.x +=1.06f*spawnPit();

					_pit = true;
				}else{
					_vtemp.x +=1.06f;
					_pit = false;
				}

					_vtemp.y =_currElevation;
					_vtemp.z -=0.001f;
				_curPlatformPrefabs.transform.position = _vtemp;//_lastPlatformPrefabs.transform.position+ (Vector3.right*1.06f) + (Vector3.up *_currElevation);


				//Spaen Enemy 

				if(GameController.Instance._dis >2  && _eCounter <= 0f){
					//Debug.Log("Enemy Spawn");
					_eCounter = Random.Range (1f,7f);
					//_eCounter = Random.Range (0.1f,0.5f);

					EnemyClass _e=  _enemyPrefab.Spawn(_curPlatformPrefabs.transform.position);
					_e.transform.parent =_curPlatformPrefabs.transform;
					_e.transform.localPosition = new Vector3(0f,1.615581f,-0.0001f);
					_e.SetBottom(_e.transform.position.y);
					_e.Reset();
					EnemyClass _eb=  _enemyBPrefab.Spawn(_curPlatformPrefabs.transform.position);
					_eb.transform.parent =_curPlatformPrefabs.transform;
					_eb.transform.localPosition = new Vector3(0f,1.615581f,-0.0001f);
					_eb.SetBottom(_eb.transform.position.y);
					_eb.Reset();

					_eb.transform.parent =null;



				}

				//spawn Coin
				if(GameController.Instance._dis >5f && _cCounter <0f){
					//_coinSpawnPackage -=1;

					_cCounter = Random.Range (0.1f, 0.2f);

					CoinClass _c = _coinPrefab.Spawn(_curPlatformPrefabs.transform.position);
					_c.transform.parent =_curPlatformPrefabs.transform;
					_c.transform.localPosition = new Vector3(0.25f,1.7f,-0.0001f);
					_c.Reset();

					_c = _coinPrefab.Spawn(_curPlatformPrefabs.transform.position);
					_c.transform.parent =_curPlatformPrefabs.transform;
					_c.transform.localPosition = new Vector3(-0.25f,1.7f,-0.0001f);
					_c.Reset();

					int rnd = Random.Range(0,100);
					if(rnd>95){
						StarClass _s = _starPrefab.Spawn(_curPlatformPrefabs.transform.position);
						_s.transform.parent =_curPlatformPrefabs.transform;
						_s.transform.localPosition = new Vector3(0.25f,2.7f,-0.0001f);
					}
//					_c = _coinPrefab.Spawn(_curPlatformPrefabs.transform.position);
//					_c.transform.parent =_curPlatformPrefabs.transform;
//					_c.transform.localPosition = new Vector3(0.25f,2.2f,-0.0001f);
//					_c.Reset();
//
//					_c = _coinPrefab.Spawn(_curPlatformPrefabs.transform.position);
//					_c.transform.parent =_curPlatformPrefabs.transform;
//					_c.transform.localPosition = new Vector3(-0.25f,2.2f,-0.0001f);
//					_c.Reset();
				}
			}
		}
		_lastPlatformPrefabs = _curPlatformPrefabs;

	}
}
