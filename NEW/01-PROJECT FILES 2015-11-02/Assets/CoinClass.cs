﻿using UnityEngine;
using System.Collections;

public class CoinClass : MonoBehaviour {
	public Animator _anim;
	public CircleCollider2D _col2d;
	private bool _isCollected =false;
	void Awake(){
		_col2d = GetComponent<CircleCollider2D> ();
	}
	// Use this for initialization
	void Start () {
		_isCollected =false;

	}

	public void Reset(){
		_col2d.enabled = false;
		_col2d.enabled = true;
		_isCollected =false;
	}
	public void CollectCoin(){
		if (_isCollected)
			return;

			AudioController.Play("sfxCollectCoin");
			_anim.Play("CoinHitEffect");
			_isCollected = true;
		
		_col2d.enabled = false;
		//transform.position = Vector3.zero;
		//Debug.Log("Coin Hilang");
		this.Recycle ();

	}
	void OnTriggerEnter2D(Collider2D obj){
		if (_isCollected)
						return;
		switch (obj.tag) {
		case "Player":
			_anim.Play("CoinHitEffect");
			_isCollected = true;
			break;
		case "EnemyHead":
			_isCollected = true;
			_col2d.enabled = false;
			this.Recycle();
			break;

		}
	}
	// Update is called once per frame
	void Update () {
		
		switch(GameController._currentGameState){
		case GameController.GameStates.InGame:

			_col2d.enabled = true;
			//transform.position -= Vector3.right * GameController._gameXSpeed * Time.deltaTime;		
			if(transform.position.x < -5f)
				this.Recycle();
			break;
		}
	}
}
