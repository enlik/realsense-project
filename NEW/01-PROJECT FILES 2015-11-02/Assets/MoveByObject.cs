﻿using UnityEngine;
using System.Collections;

public class MoveByObject : MonoBehaviour {

	public Vector3 bounceDistance;
	public float bounceTime;
	public iTween.EaseType easeType;
	public iTween.LoopType loopType;
	
	private GameObject myGameObject;
	private Transform myTransform;
	private Vector3 initialPosition;

	#region Init
	private void Awake () {
		myGameObject = gameObject;
		myTransform = transform;
		initialPosition = myTransform.localPosition;
	}

	private void OnEnable () {
		myTransform.localPosition = initialPosition;
		Bounce();
	}

	private void OnDisable () {
		iTween.Stop(myGameObject);
	}
	#endregion


	#region CORE
	private void Bounce () {

		iTween.MoveBy(myGameObject, iTween.Hash(
			"amount", bounceDistance,
			"time", bounceTime,
			"looptype", loopType,
			"easetype", easeType
			));
	}
	#endregion
}
