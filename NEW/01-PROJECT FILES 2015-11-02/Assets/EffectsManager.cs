﻿using UnityEngine;
using System.Collections;

public class EffectsManager : MonoBehaviour {
	

	public HitEffectClass _hitEffect;
	private static EffectsManager _instance;
	public static EffectsManager Instance{
		get{
			return _instance;		
		}
	}

	
	void Awake(){
		_instance = this;
		_hitEffect.CreatePool ();
	}

	// Use this for initialization
	void Start () {
	
	}
	public void ShowEffect(Vector3 pos){
		HitEffectClass eff = _hitEffect.Spawn (pos+ Vector3.forward);
		eff.PlayEffect ();
	}
	// Update is called once per frame
	void Update () {
	
	}
}
