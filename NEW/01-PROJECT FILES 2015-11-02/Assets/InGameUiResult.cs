﻿using UnityEngine;
using System.Collections;

public class InGameUiResult : MonoBehaviour {

	public GameObject _container;
	void OnEnable(){
		EventManager.onChangeGameStateEvent += HandlerChangeGameStateEvent;
	}
	void OnDisable(){
		EventManager.onChangeGameStateEvent -= HandlerChangeGameStateEvent;
	}
	

	// Use this for initialization
	void Start () {
		_container.SetActive (false);
	}
	private void HandlerChangeGameStateEvent(GameController.GameStates state){
		switch(state){
		case GameController.GameStates.GameOver:
			_container.SetActive (true);
			break;
		default:
			break;
		}
		
	}

	// Update is called once per frame
	void Update () {
	
	}
}
