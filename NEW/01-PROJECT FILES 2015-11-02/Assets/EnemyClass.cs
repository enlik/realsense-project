﻿using UnityEngine;
using System.Collections;

public class EnemyClass : MonoBehaviour {
	public BoxCollider2D _col;
	public BoxCollider2D _col2;
	public Animator _anim;


	private float _jumpPower = 4f;
	public int _jumpCount = 0;
	
	private float _grav = 5f;
	public float _yvel = 0f;
	public float _yvelMax = 7f;
	public float _yvelMin = -7f;
	public float _bottom = -0.93f;
	private bool _isJumpRestored = true;
	private bool _isJumping   = false;

	private Vector3 _tempPos;
	private bool _isDropped = false;
	private bool _isFall = false;
	private bool _isDead = false;
	// Use this for initialization
	void Start () {
		_col.enabled = false;
		_col2.enabled = false;

	}

	void OnTriggerEnter2D(Collider2D obj){
		Debug.Log ("obj.tag " + obj.tag);
	}
	public void SetBottom(float xPos){
		_bottom = xPos;
	}
	public void Reset(){
		_col.enabled = false;
		_col.enabled = true;
		_col2.enabled = false;
		_col2.enabled = true;
	}
	void UpdateJump (){
		if (!_isJumping)
			return;
		
		if (_yvel > _yvelMax)
			_yvel = _yvelMax;
		if(_yvel < _yvelMin)
			_yvel = _yvelMin;
		
		if (!_isFall && _yvel < 0f) {
			_isFall = true;
//			if(!_isDead)
//				_anim.Play("Blobby_Fall");
		}



		_tempPos = transform.position;
		_yvel -= _grav *Time.deltaTime;
		_tempPos.y += _yvel*Time.deltaTime;
		if (_tempPos.y <= _bottom) {
			_tempPos.y =_bottom;
			_isJumping = false;
			StartCoroutine(RestoreJump());
			_anim.Play ("Bolbob_Idle");
		}
		transform.position = _tempPos;

	}
	IEnumerator RestoreJump(){
		yield return new WaitForSeconds (Random.Range (0.5f, 1f));
		_isJumpRestored = true;		
	}
	public void Jump(){

		

		//		if (_isJumping)
		//			return;
		if (!_isJumpRestored)
			return;
		_anim.Play ("Bolbob_Jump");
		_isFall = false;
		_isJumpRestored = false;
		_isJumping = true;
		_yvel = _jumpPower;

		//Debug.Log("Jumppmpmpmpmpm");
	}


	// Update is called once per frame
	void Update () {

		switch(GameController._currentGameState){
		case GameController.GameStates.InGame:
			_col.enabled = true;
			_col2.enabled = true;
			//transform.position -= Vector3.right * GameController._gameXSpeed * Time.deltaTime;		
			if(transform.position.x < -5f)
				this.Recycle();


			Jump();
			UpdateJump();

			break;
		}
	}
}
