﻿using UnityEngine;
using System.Collections;

public class HitEffectClass : MonoBehaviour {
	private Animator _anim;
	void Awake(){
		_anim = GetComponent<Animator> ();
	}
	// Use this for initialization
	void Start () {
	
	}
	public void PlayEffect(){
		_anim.Play("HitEffect");
		StartCoroutine (RecycleEffect ());
	}
	IEnumerator RecycleEffect(){
		yield return new WaitForSeconds (2f);
		this.Recycle ();
	}
	// Update is called once per frame
	void Update () {
		switch(GameController._currentGameState){
		case GameController.GameStates.InGame:
			transform.position -= Vector3.right * GameController._gameXSpeed * Time.deltaTime;		
			if(transform.position.x < -6f)
				this.Recycle();
			break;
		}
	}
}
