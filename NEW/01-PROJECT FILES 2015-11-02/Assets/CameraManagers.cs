﻿using UnityEngine;
using System.Collections;

public class CameraManagers : MonoBehaviour {
	public Transform _player ;

	public TextMesh _textTutorial;
	public GameObject _gtextTutorial;
	private bool _isTrackPlayer =true;
	private bool _isMoveWithBackGround =false;
	private Vector3 _tempPos;
	private float _ymin = -1.5f;
	private Camera _cam;



	void Awake(){
		_cam = GetComponent<Camera> ();

#if ANDROID
		_textTutorial.text ="Touch to JUMP";
#endif
	}
	// Use this for initialization
	void Start () {
		StartCoroutine (HideTutorial ());
	}
	IEnumerator HideTutorial(){
		yield return new WaitForSeconds (3f);
		_gtextTutorial.SetActive (false);
	}
	public void ChangeSize(GameController.GameSpeedMode gameSpeed){

		switch(gameSpeed){
		case GameController.GameSpeedMode.Stop:
			StartCoroutine(ChangeCamSize(2f));

			//_cam.orthographicSize = 2f;
			break;
		case GameController.GameSpeedMode.Slow:
			StartCoroutine(ChangeCamSize(2.4f));

			//_cam.orthographicSize = 2f;
			break;
		case GameController.GameSpeedMode.Fast:
			StartCoroutine(ChangeCamSize(2.6f));

			//_cam.orthographicSize = 2.2f;
			break;
		case GameController.GameSpeedMode.Faster:
			StartCoroutine(ChangeCamSize(2.8f));

			//_cam.orthographicSize = 2.4f;
			break;
		case GameController.GameSpeedMode.Crazy:
			StartCoroutine(ChangeCamSize(2.8f));
		
			break;
		}
	}
	IEnumerator ChangeCamSize(float camSize){
		float ftime = 0f;
		float cur = _cam.orthographicSize;
		float to = camSize;

		while (ftime <1f) {
			_cam.orthographicSize = Mathf.Lerp(cur , to , ftime);
			ftime += 1f *Time.deltaTime;
			yield return new WaitForSeconds (0.01f);

		}
	}
	// Update is called once per frame
	void Update () {
		_tempPos = transform.position;
		if (Input.GetKeyDown (KeyCode.A)) {
			_isMoveWithBackGround = true;
		}
		if (_isTrackPlayer) {


			_tempPos.y = _player.position.y;
			if(_tempPos.y> _ymin)
				transform.position = _tempPos;
		
		}

		if (_isMoveWithBackGround) {
			transform.position  -= Vector3.right * GameController._gameXSpeed * Time.deltaTime;


		}

	}
}
