﻿using UnityEngine;
using System.Collections;

public class BlobbyClass : MonoBehaviour {
	public float _jumpPower = 1f;
	public int _jumpCount = 0;

	public float _grav = 0.9f;
	public float _yvel = 0f;
	public float _yvelMax = 7f;
	public float _yvelMin = -7f;

	public float _rayDistance =0.25f;
	public float _rayDistanceEnemy = 0.05f;
	private float _bottom = -0.93f;
	private Animator _anim;

	private bool _isDropped = false;
	private bool _isFall = false;
	private bool _isDead = false;

	private Vector3 _tempPos;
	private bool _isJumpRestored = true;
	private bool _isJumping   = false;
	private Transform _platform ;

	public GameObject _hyperPartcle ;
	private bool _hyperMode =false;
	enum PlayerState{
		Walk,
		Run,
		Dash,
		Craze

	}
	void Awake(){
		_anim = GetComponent<Animator> ();
	}
	// Use this for initialization
	void Start () {
		_tempPos = transform.position;
		_isJumping = true;
		SetHyperModeOff ();
	}
	public void SetHyperModeOn(){
		if (!_hyperMode)
		{
			Debug.Log("HyperMode On+++++++++++++++++++++");

			AudioController.Play("sfxCollectCoin");
			_hyperMode = true;
			_hyperPartcle.SetActive (true);
			StartCoroutine (PerformHyperMode ());
		}
	}
	IEnumerator PerformHyperMode(){
		yield return new WaitForSeconds (3f);//durration
		SetHyperModeOff ();
	}
	public void SetHyperModeOff(){
		Debug.Log("HyperMode Off**************");

		_hyperMode = false;
		_hyperPartcle.SetActive (false);
	}
	#region Animation 
	public void ChangeAnimation(GameController.GameSpeedMode speedMode){
		if (_isDead)
			return;
		switch(speedMode){
		case GameController.GameSpeedMode.Slow:
			_anim.Play("Blobby_walk");
			break;
		case GameController.GameSpeedMode.Fast:
			_anim.Play("Blobby_Dash");
			break;
		case GameController.GameSpeedMode.Faster:
			_anim.Play("Blobby_Sprint");
			break;
		case GameController.GameSpeedMode.Crazy:
			_anim.Play("Blobby_Craze");
			break;
		}
	}
	#endregion
	#region Trigger
	public void Jump(){
		if (_jumpCount >= 2)
						return;
		if (_jumpCount == 0)
		_anim.Play("Blobby_Jump");
		else if (_jumpCount >= 1)
			_anim.Play("Blobby_Craze");

		AudioController.Play("sfxJump");
//		if (_isJumping)
//			return;
		if (!_isJumpRestored)
			return;
		_jumpCount++;
		_isFall = false;
		_isJumpRestored = false;
		_isJumping = true;
		_yvel = _jumpPower;
		StartCoroutine (RestoreJump ());
		//Debug.Log("Jumppmpmpmpmpm");
	}
	IEnumerator RestoreJump(){
		yield return new WaitForSeconds (0.2f);
		_isJumpRestored = true;
	}

//	public void OnTriggerStay2D( Collider2D obj) {
//		if (!_isJumpRestored)
//			return;
//		switch (obj.tag) {
//		case "Platform":
//			if(_isJumping){
//				_isJumping = false;
//			}
//			break;
//
//		}
//	}
//	public void OnTriggerExit2D( Collider2D obj) {
//		switch (obj.tag) {
//		case "Platform":
//			if(!_isJumping){
//				_isJumping = true;
//			}
//
//			break;
//			
//		}
//	}
//
//	public void OnTriggerEnter2D( Collider2D obj) {
//		if (!_isJumpRestored)
//						return;
//		switch (obj.tag) {
//		case "Platform":
//			//if(_isJumping){
//				Debug.Log("Landed");
//				_isJumping = false;
//				//_tempPos.y = _bottom;
//
//				_tempPos.y = obj.transform.position.y ;
//				transform.position = _tempPos;
//			//}
//			break;
//		}
//	}
	#endregion
	void UpdateJump (){
		if (!_isJumping)
						return;


		if (_yvel > _yvelMax)
						_yvel = _yvelMax;
		if(_yvel < _yvelMin)
			_yvel = _yvelMin;

		if (!_isFall && _yvel < 0f) {
			_isFall = true;
			if(!_isDead)
			_anim.Play("Blobby_Fall");
		}
		
		_tempPos = transform.position;

		_yvel -= _grav *Time.deltaTime;
		
	
		_tempPos.y += _yvel*Time.deltaTime;

//		if (_tempPos.y <_bottom) {
//			_yvel = 0f;
//			//_yvel *=  -0.5f *Time.deltaTime;
//			_tempPos.y = _bottom;
//		}
//		if(_tempPos.y ==_bottom){
//			if(_yvel <= 0f){// -0.16666666666666669){
//				_isDropped = true;
//				_isJumping = false;
//			}
//		}

		transform.position = _tempPos;
//		if(_platform){
//			Vector3 tempPlatformPos = _platform.position;
//
//			tempPlatformPos.y = _tempPos.y-0.2f;
//			_platform.position = tempPlatformPos;
//		}
	}
	void SetDead(){
		_isDead = true;
		GameController.Instance.SetGameOver();
		_anim.Play("Blobby_Dead");

	}
	void UpdateCheckHitEnemy(){
		RaycastHit2D hit  = Physics2D.Raycast(new Vector2(transform.position.x,transform.position.y) , Vector2.right, _rayDistanceEnemy);
		if (hit.collider != null && !_isDead) {
			switch (hit.collider.tag) {
			case "Coin":
				//Debug.Log("coin 0 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				EffectsManager.Instance.ShowEffect(hit.transform.position);

				hit.transform.GetComponent<CoinClass>().CollectCoin();
			
				//AudioController.Play("sfxCollectCoin");
				break;
			case "EnemyBody":
				if(_hyperMode){
					Debug.Log("name = " + hit.transform.parent.parent.name);
					if(hit.transform.parent.parent.GetComponent<EnemyClass>()){
						EnemyClass ec =hit.transform.parent.parent.GetComponent<EnemyClass>();
						hit.transform.parent.parent.GetComponent<EnemyClass>()._anim.Play("Bolbob_Dead");
						ec.SetBottom(-100f);
						EffectsManager.Instance.ShowEffect(new Vector3(hit.transform.position.x ,
						                                               hit.transform.position.y , 
						                                               -2f ) );
					}
					
					if(_isJumping && _isJumpRestored){	
						
						//Debug.Log("Land +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
						_isJumping = false;
						_isFall = false;
						ChangeAnimation(GameController._currGameSpeedMode);
						_jumpCount =0;
						_yvel = 0f;
						_tempPos.y = hit.collider.transform.position.y ;
						//_platform = hit.collider.transform.parent;
						transform.position = _tempPos;
						//				transform.position = _tempPos;
						
					}
					if(_tempPos.y != hit.collider.transform.position.y && _isJumpRestored){
						_tempPos.y = hit.collider.transform.position.y ;
						transform.position = _tempPos;
						
						
					}
					AudioController.Play("sfxEnemyKill");
				}else{
					AudioController.Play("sfxDead");
					//Debug.Log("EnemyBody +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
										_isDead = true;
										GameController.Instance.SetGameOver();
										_anim.Play("Blobby_Dead");
				}
				break;
			}

		}
	
	}
	void ReCheckPlatform(){
		RaycastHit2D hit  = Physics2D.Raycast(new Vector2(transform.position.x,transform.position.y) , -Vector2.up, _rayDistance);
		if (!hit)
						return;
		switch (hit.collider.tag) {
		case "Coin":
			Debug.Log("Coin Again +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

			EffectsManager.Instance.ShowEffect(hit.transform.position);
			
			hit.transform.GetComponent<CoinClass>().CollectCoin();
			if(!hit)
				hit.transform.GetComponent<CoinClass>().Recycle();
		

			break;
		case "Star":
			hit.transform.Recycle();
			SetHyperModeOn();
			break;
		case "Platform":
			//Debug.Log("    0                  ");

			//Debug.Log("========================================================================");

			if(_isJumping && _isJumpRestored){	
				
				//Debug.Log("Land +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				_isJumping = false;
				_isFall = false;
				ChangeAnimation(GameController._currGameSpeedMode);
				_jumpCount =0;
				_yvel = 0f;
				_tempPos.y = hit.collider.transform.position.y ;
				//_platform = hit.collider.transform.parent;
				transform.position = _tempPos;
				//				transform.position = _tempPos;
				
			}
			if(_tempPos.y != hit.collider.transform.position.y && _isJumpRestored){
				_tempPos.y = hit.collider.transform.position.y ;
				transform.position = _tempPos;
				
				
			}
			
			break;
		}
	}
	// Update is called once per frame
	void Update () {
		UpdateCheckHitEnemy ();
		if (_hyperMode)
			_hyperPartcle.SetActive (true);
		else
			_hyperPartcle.SetActive (false);
		RaycastHit2D hit  = Physics2D.Raycast(new Vector2(transform.position.x,transform.position.y) , -Vector2.up, _rayDistance);
		if (hit.collider != null && !_isDead) {
			switch (hit.collider.tag) {
				case "Platform":
					
					if(_isJumping && _isJumpRestored){	

						//Debug.Log("Land +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
						_isJumping = false;
						_isFall = false;
						ChangeAnimation(GameController._currGameSpeedMode);
						_jumpCount =0;
						_yvel = 0f;
						_tempPos.y = hit.collider.transform.position.y ;
						//_platform = hit.collider.transform.parent;
						transform.position = _tempPos;
						//				transform.position = _tempPos;

					}
					if(_tempPos.y != hit.collider.transform.position.y && _isJumpRestored){
						_tempPos.y = hit.collider.transform.position.y ;
						transform.position = _tempPos;
						

					}

					break;
				case "Coin":
					Debug.Log("Coin 1 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
					
					EffectsManager.Instance.ShowEffect(hit.transform.position);
				
					hit.transform.GetComponent<CoinClass>().CollectCoin();
					if(!hit)
					hit.transform.GetComponent<CoinClass>().Recycle();
//					hit.transform.GetComponent<CoinClass>().CollectCoin();
//					//AudioController.Play("sfxCollectCoin");
				break;
				case "Star":
					hit.transform.Recycle();
					SetHyperModeOn();
					break;
				case "EnemyHead":
				//Musuh Mati
					Jump();
				Debug.Log("Lompat" + hit.transform.name);
//
				Debug.Log("Punya"+ hit.transform.GetComponent<EnemyClass>().name);

				if(hit.transform.GetComponent<EnemyClass>()){
					EnemyClass ec =hit.transform.GetComponent<EnemyClass>();


					 hit.transform.GetComponent<EnemyClass>()._anim.Play("Bolbob_Dead");
					ec.SetBottom(-100f);
					EffectsManager.Instance.ShowEffect(new Vector3(hit.transform.position.x ,
					                                               hit.transform.position.y , 
					                                               -2f ) );
				}
					
					_jumpCount = 1;
					AudioController.Play("sfxEnemyKill");
				break;
				case "PitFall":
				AudioController.Play("sfxDead");
					Debug.Log("PitFall +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
					_isDead = true;
					GameController.Instance.SetGameOver();
					_anim.Play("Blobby_Dead");
				break;
				case "Obstacle":
					//Debug.Log("Obstacle *********************************************************");
					AudioController.Play("sfxBumped");
					Jump();
					GameController.Instance.SetSpeedSlow();
					//gameover script
//					_isDead = true;
//					GameController.Instance.SetGameOver();
//					_anim.Play("Blobby_Dead");
//
					break;
				default:
					_isJumping = true;


				break;
			}
			ReCheckPlatform();
		}else{
			//Debug.Log("space");
			_isJumping = true;
			

		}

		UpdateJump ();
		if (_isDead && GameController._currentGameState == GameController.GameStates.GameOver) {
			if(Input.GetKeyDown(KeyCode.Space)){
				Application.LoadLevel(0);
				//EventManager.ChangeGameStateMethod(GameController.GameStates.GameOver);
			}
			for (var i = 0; i < Input.touchCount; ++i) {
				if(i>1)
					return;
				if (Input.GetTouch(i).phase == TouchPhase.Began){
					
					Application.LoadLevel(0);
					//EventManager.ChangeGameStateMethod(GameController.GameStates.GameOver);
				}
			}
				
		}else if (_isDead) {
			if(Input.GetKeyDown(KeyCode.Space)){
				//Application.LoadLevel(0);
				EventManager.ChangeGameStateMethod(GameController.GameStates.GameOver);
			}
			for (var i = 0; i < Input.touchCount; ++i) {
				if(i>1)
					return;
				if (Input.GetTouch(i).phase == TouchPhase.Began){

					//Application.LoadLevel(0);
					EventManager.ChangeGameStateMethod(GameController.GameStates.GameOver);
				}
			}
		}
	}
}
