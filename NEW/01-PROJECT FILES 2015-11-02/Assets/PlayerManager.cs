﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {
	public BlobbyClass _player;
	public Animator _anim;

	private static PlayerManager _instance;
	public static PlayerManager Instance{
		get{
			return _instance;		
		}
	}
	void Awake(){
		_instance = this;
	}
	// Use this for initialization
	void Start () {
	
	}
	public void ChangeAnimation(GameController.GameSpeedMode speedMode){
	
		_player.ChangeAnimation(speedMode);
	}
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			//Debug.Log ("Jump+++++++ " );
			_player.Jump();
		}
		for (var i = 0; i < Input.touchCount; ++i) {
			if(i>1)
				return;
			if (Input.GetTouch(i).phase == TouchPhase.Began){
				//Debug.Log ("Jump+++++++ " );
				_player.Jump();
			}
		}
	}
}
