﻿using UnityEngine;
using System.Collections;

public class PlatformsManagers : MonoBehaviour {
	public GameObject _platformPrefabs;
	public GameObject _lastPlatformPrefabs;
	public GameObject _curPlatformPrefabs;

	private float _CreatePlatformCounter ;
	private float _CreatePlatformCounterBase = 1f;
	private Vector3 _vtemp;
	private float[] _elevations = {
		-2.5f,
		-2f,
		-1.5f,
		-1f,
		0f,
		0.5f,
		1f
	};
	private	int _rnd = 0;
	private int _lstRnd= 0;

	private float _currElevation  ;

	private  void Awake(){
		_CreatePlatformCounter = _CreatePlatformCounterBase;
		_platformPrefabs.CreatePool ();
		_currElevation = _elevations [0];
	}


	// Use this for initialization
	private  void Start () {
	
	}

	// Update is called once per frame
	private void Update () {
		_CreatePlatformCounter -= GameController._gameXSpeed * Time.deltaTime;
		//Debug.Log ("_CreatePlatformCounter " + _CreatePlatformCounter + " _currElevation " +_currElevation);
		//ganti posisi y 
		if (_CreatePlatformCounter <= 0) {
			_CreatePlatformCounter = Random.Range(5f, _CreatePlatformCounterBase);
			_rnd = Random.Range(0,3);
			_currElevation = _elevations[_rnd];
			switch(_lstRnd){
				case 2:
					_rnd = Random.Range(0,4);
					_currElevation = _elevations[_rnd];
					//Debug.Log ("_currElevation " + _rnd + " ===========================");
					break;
				case 3:
					_rnd = Random.Range(0,5);
					_currElevation = _elevations[_rnd];
					//Debug.Log ("_currElevation " + _rnd + " ===========================");
					break;
				case 4:
					_rnd = Random.Range(0,6);
					_currElevation = _elevations[_rnd];
					//Debug.Log ("_currElevation " + _rnd + " ===========================");
					break;
				case 5:
					_rnd = Random.Range(0,7);
					_currElevation = _elevations[_rnd];
					//Debug.Log ("_currElevation " + _rnd + " ===========================");
					break;
				case 6:
					_rnd = Random.Range(0,3);
					_currElevation = _elevations[_rnd];
					Debug.Log ("_currElevation " + _rnd + " ===========================");
					break;
			}

			//Debug.Log ("_currElevation " + _currElevation + " ===========================");
			_lstRnd = _rnd;
		}
			if(_lastPlatformPrefabs==null){
			_curPlatformPrefabs = _platformPrefabs.Spawn(new Vector3(10f,_currElevation,0f));
			}else{
				if(_lastPlatformPrefabs.transform.position.x < 10f-1.1f){
					_curPlatformPrefabs = _platformPrefabs.Spawn(new Vector3(10f,_currElevation,0f));
					//_curPlatformPrefabs.transform.parent = _lastPlatformPrefabs.transform;
					_vtemp = _lastPlatformPrefabs.transform.position;
					_vtemp.x +=1.06f;
					_vtemp.y =_currElevation;

				_curPlatformPrefabs.transform.position = _vtemp;//_lastPlatformPrefabs.transform.position+ (Vector3.right*1.06f) + (Vector3.up *_currElevation);
					//_curPlatformPrefabs.transform.parent = null;
				}
			}
			_lastPlatformPrefabs = _curPlatformPrefabs;

	}
}
