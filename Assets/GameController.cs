﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
	public BackgroundManager _bgMng;
	public PlatformsManagers _plMng;
	public CameraManagers _cmrMng;
	public PlayerManager _plyMng;
	public TextMesh  _textDistances;
	public float _dis =0;
	public float _changeSpeedCountDown ;
	public float _changeSpeedCountDownTime = 5f;



	//GameStates
	public enum GameStates {
		Menu,
		TitleScreen,
		GameOver,
		InGame,
		Pause

	}
	public enum GameSpeedMode {
		Stop,
		Slow,
		Fast,
		Faster,
		Crazy
	
		
	}
	public static GameStates _currentGameState;
	public static GameSpeedMode _currGameSpeedMode;

	//Game Stats
	public  float _gameSpeed = 1f;
	public static float _gameXSpeed = 1f;
	public static float _gameYSpeed = 0f;

	private float _speedStop = 0f;
	private float _speedSlow = 1f; 
	private float _speedFast = 2f;
	private float _speedFaster = 4f;
	private float _speedCrazy = 8f;



	private static GameController _instance;
	public static GameController Instance{
		get{
			return _instance;		
		}
	}
	public float GameXSpeed{
		set{
			_gameXSpeed = _gameSpeed  = value;
		
		}
	}

	void Awake(){
		_instance = this;
		_currentGameState = GameStates.InGame;
		_currGameSpeedMode = GameSpeedMode.Slow;

		_changeSpeedCountDown = _changeSpeedCountDownTime;
	}
	public void SetGameOver(){

		_currentGameState = GameStates.GameOver;
		_currGameSpeedMode = GameSpeedMode.Stop;
		_cmrMng.ChangeSize(_currGameSpeedMode);

	}
	// Use this for initialization
	void Start () {
		_currGameSpeedMode = GameSpeedMode.Slow;
		CheckGameSpeed ();
	}
	void CheckGameSpeed(){
		switch(_currGameSpeedMode){
		case GameSpeedMode.Stop:
			_gameSpeed =_speedStop;
			break;
		case GameSpeedMode.Slow:
			_gameSpeed =_speedSlow;
			_changeSpeedCountDown -= 1f * Time.deltaTime;
			if(_changeSpeedCountDown<=0f){
//				_cmrMng.ChangeSize(_currGameSpeedMode);
//
//				_currGameSpeedMode = GameSpeedMode.Fast;
//				PlayerManager.Instance.ChangeAnimation(_currGameSpeedMode);
//				_changeSpeedCountDown = 5f;
//				_gameSpeed = _speedFast;
			}
			break;
		case GameSpeedMode.Fast:
			_changeSpeedCountDown -= 1f * Time.deltaTime;
			if(_changeSpeedCountDown<=0f){
//				_currGameSpeedMode = GameSpeedMode.Faster;
//				PlayerManager.Instance.ChangeAnimation(_currGameSpeedMode);
//				_cmrMng.ChangeSize(_currGameSpeedMode);
//
//				_changeSpeedCountDown = 8f;
//
//				_gameSpeed = _speedFaster;
			}
			break;
		case GameSpeedMode.Faster:
			_changeSpeedCountDown -= 1f * Time.deltaTime;
			if(_changeSpeedCountDown<=0f){
				_currGameSpeedMode = GameSpeedMode.Crazy;
				PlayerManager.Instance.ChangeAnimation(_currGameSpeedMode);
				_cmrMng.ChangeSize(_currGameSpeedMode);

				_gameSpeed =_speedCrazy;
				_changeSpeedCountDown = 10f;

			}
			break;
		case GameSpeedMode.Crazy:

			break;
		}
	}
	// Update is called once per frame
	void Update () {

		CheckGameSpeed ();
		_gameXSpeed = _gameSpeed;
		
		if (_currentGameState != GameStates.GameOver) {
			_dis += 0.5f* _gameXSpeed *Time.deltaTime;

			_textDistances.text ="Distances : "+ Mathf.Round (_dis) +" m ";

		}
	}
}
