﻿using UnityEngine;
using System.Collections;

public class PlatformClass : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		switch(GameController._currentGameState){
		case GameController.GameStates.InGame:
			transform.position -= Vector3.right * GameController._gameXSpeed * Time.deltaTime;		
			if(transform.position.x < -15f)
				this.Recycle();
			break;
		}
	}
}
