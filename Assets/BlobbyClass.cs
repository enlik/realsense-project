﻿using UnityEngine;
using System.Collections;

public class BlobbyClass : MonoBehaviour {
	public float _jumpPower = 1f;
	public int _jumpCount = 0;

	public float _grav = 0.9f;
	public float _yvel = 0f;
	public float _yvelMax = 7f;
	public float _yvelMin = -7f;

	public float _rayDistance = 0.2f;
	
	private float _bottom = -0.93f;
	private Animator _anim;

	private bool _isDropped = false;
	private bool _isFall = false;
	private bool _isDead = false;

	private Vector3 _tempPos;
	private bool _isJumpRestored = true;
	private bool _isJumping   = false;
	private Transform _platform ;
	enum PlayerState{
		Walk,
		Run,
		Dash,
		Craze

	}
	void Awake(){
		_anim = GetComponent<Animator> ();
	}
	// Use this for initialization
	void Start () {
		_tempPos = transform.position;
		_isJumping = true;
	}
	#region Animation 
	public void ChangeAnimation(GameController.GameSpeedMode speedMode){
		switch(speedMode){
		case GameController.GameSpeedMode.Slow:
			_anim.Play("Blobby_walk");
			break;
		case GameController.GameSpeedMode.Fast:
			_anim.Play("Blobby_Dash");
			break;
		case GameController.GameSpeedMode.Faster:
			_anim.Play("Blobby_Sprint");
			break;
		case GameController.GameSpeedMode.Crazy:
			_anim.Play("Blobby_Craze");
			break;
		}
	}
	#endregion
	#region Trigger
	public void Jump(){
		if (_jumpCount >= 2)
						return;
		_anim.Play("Blobby_Jump");
//		if (_isJumping)
//			return;
		if (!_isJumpRestored)
			return;
		_jumpCount++;
		_isFall = false;
		_isJumpRestored = false;
		_isJumping = true;
		_yvel = _jumpPower;
		StartCoroutine (RestoreJump ());
		//Debug.Log("Jumppmpmpmpmpm");
	}
	IEnumerator RestoreJump(){
		yield return new WaitForSeconds (0.2f);
		_isJumpRestored = true;
	}

//	public void OnTriggerStay2D( Collider2D obj) {
//		if (!_isJumpRestored)
//			return;
//		switch (obj.tag) {
//		case "Platform":
//			if(_isJumping){
//				_isJumping = false;
//			}
//			break;
//
//		}
//	}
//	public void OnTriggerExit2D( Collider2D obj) {
//		switch (obj.tag) {
//		case "Platform":
//			if(!_isJumping){
//				_isJumping = true;
//			}
//
//			break;
//			
//		}
//	}
//
//	public void OnTriggerEnter2D( Collider2D obj) {
//		if (!_isJumpRestored)
//						return;
//		switch (obj.tag) {
//		case "Platform":
//			//if(_isJumping){
//				Debug.Log("Landed");
//				_isJumping = false;
//				//_tempPos.y = _bottom;
//
//				_tempPos.y = obj.transform.position.y ;
//				transform.position = _tempPos;
//			//}
//			break;
//		}
//	}
	#endregion
	void UpdateJump (){
		if (!_isJumping)
						return;


		if (_yvel > _yvelMax)
						_yvel = _yvelMax;
		if(_yvel < _yvelMin)
			_yvel = _yvelMin;

		if (!_isFall && _yvel < 0f) {
			_isFall = true;
			_anim.Play("Blobby_Fall");
		}
		
		_tempPos = transform.position;

		_yvel -= _grav *Time.deltaTime;
		
	
		_tempPos.y += _yvel*Time.deltaTime;

//		if (_tempPos.y <_bottom) {
//			_yvel = 0f;
//			//_yvel *=  -0.5f *Time.deltaTime;
//			_tempPos.y = _bottom;
//		}
//		if(_tempPos.y ==_bottom){
//			if(_yvel <= 0f){// -0.16666666666666669){
//				_isDropped = true;
//				_isJumping = false;
//			}
//		}

		transform.position = _tempPos;

		//mentalin platform
//
//		if(_platform){
//			Vector3 tempPlatformPos = _platform.position;
//
//			tempPlatformPos.y = _tempPos.y-0.2f;
//			_platform.position = tempPlatformPos;
//		}
	}


	// Update is called once per frame
	void Update () {

		RaycastHit2D hit  = Physics2D.Raycast(new Vector2(transform.position.x,transform.position.y) , -Vector2.up, _rayDistance);
		if (hit.collider != null && !_isDead) {
			switch (hit.collider.tag) {
				case "Platform":
					
					if(_isJumping && _isJumpRestored){	

						//Debug.Log("Land +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
						_isJumping = false;
						_isFall = false;
						ChangeAnimation(GameController._currGameSpeedMode);
						_jumpCount =0;
						_yvel = 0f;
						_tempPos.y = hit.collider.transform.position.y ;
						
					    //ambil platform yang di injek di sini 
						//_platform = hit.collider.transform.parent;
						transform.position = _tempPos;
						//transform.position = _tempPos;
					}
					if(_tempPos.y != hit.collider.transform.position.y){
						_tempPos.y = hit.collider.transform.position.y ;
						transform.position = _tempPos;
					}
					break;
				case "Obstacle":
					Debug.Log("Obstacle *********************************************************");
					_isDead = true;
					GameController.Instance.SetGameOver();
					break;
				default:
					_isJumping = true;


				break;
			}
		}else{
			//Debug.Log("space");
			_isJumping = true;
			

		}

		UpdateJump ();

		if (_isDead) {
			if(Input.GetKeyDown(KeyCode.Space)){
				Application.LoadLevel(0);
			}
			for (var i = 0; i < Input.touchCount; ++i) {
				if(i>1)
					return;
				if (Input.GetTouch(i).phase == TouchPhase.Began){

					Application.LoadLevel(0);
				}
			}
		}
	}
}
